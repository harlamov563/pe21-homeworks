function setContentSwitcherPair(switcherID, contentID) {

    const tabSwitcher = document.getElementById(switcherID);
    const tabItems = [...tabSwitcher.children];
    const tabContent = [...document.getElementById(contentID).children];


    // asigning IDs for tabs
    tabItems.forEach(tabItem => tabItem.id = `${tabItem.textContent.replace(/\s+/g, '').toLowerCase()}Tab`);
    tabContent.forEach(tab => tab.id = tab.previousSibling.previousSibling.textContent.replace(/\s+/g, '').toLowerCase());

    tabItems.forEach(tab => tab.className = `tabs-title`);
    tabItems[0].className = 'tabs-title active';

    tabItems.forEach(tab => {
        const contentTab = document.getElementById(`${tab.id.replace('Tab','')}`);
        tab.className === `tabs-title active` ? contentTab.hidden = false : contentTab.hidden = true;
    });
}

tabSwitcher.addEventListener('click', ev => {
    const tabs = [...ev.currentTarget.children]
    const tabsContent = [...ev.currentTarget.nextElementSibling.children];
    const currentTabIndex = tabs.indexOf(ev.target);

    tabs.forEach(tab => tab.className = 'tabs-title');

    if(tabs[currentTabIndex].id.replace('Tab','') === tabsContent[currentTabIndex].id) {
        tabs[currentTabIndex].classList.add('active');
        tabsContent.forEach(tab => tab.hidden = true);
        tabsContent[currentTabIndex].hidden = false;

    } else {
        const activeContent = document.getElementById(`${tabs[currentTabIndex].id.replace('Tab','')}`);
        tabs[currentTabIndex].classList.add('active');
        tabsContent.forEach(tab => tab.hidden = true);
        activeContent.hidden = false;
    }
});

setContentSwitcherPair('tabSwitcher','tabContent');