function setContentSwitcherPair(switcherID, contentID) {

    const tabSwiter = document.getElementById(switcherID);
    const tabItems = [...tabswiter.children];
    const tabContic = [...document.getElementById(contentID).children];


    // asigning IDs for tabs
    tabItems.forEach(tabItem => tabItem.id = `${tabItem.textContent.replace(/\s+/g, '').toLowerCase()}Tab`);
    tabContic.forEach(tab => tab.id = tab.previousSibling.previousSibling.textContent.replace(/\s+/g, '').toLowerCase());

    tabItems.forEach(tab => tab.className = `tabs-title`);
    tabItems[0].className = 'tabs-title active';

    tabItems.forEach(tab => {
        const contentTab = document.getElementById(`${tab.id.replace('Tab','')}`);
        tab.className === `tabs-title active` ? contentTab.hidden = false : contentTab.hidden = true;
    });
}

tabSwiter.addEventListener('click', ev => {
    const tabs = [...ev.currentTarget.children]
    const tabsContic = [...ev.currentTarget.nextElementSibling.children];
    const currentTabIndex = tabs.indexOf(ev.target);

    tabs.forEach(tab => tab.className = 'tabs-title');

    if(tabs[currentTabIndex].id.replace('Tab','') === tabsContic[currentTabIndex].id) {
        tabs[currentTabIndex].classList.add('active');
        tabsContic.forEach(tab => tab.hidden = true);
        tabsContic[currentTabIndex].hidden = false;

    } else {
        const activeContent = document.getElementById(`${tabs[currentTabIndex].id.replace('Tab','')}`);
        tabs[currentTabIndex].classList.add('active');
        tabsContic.forEach(tab => tab.hidden = true);
        activeContent.hidden = false;
    }
});

setContentSwitcherPair('tabSwiter','tabContic');