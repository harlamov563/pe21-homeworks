function highligt() {
    document.body.addEventListener('keydown', press => {
        const buttons = Array.from(document.getElementsByTagName('button'));
        switch(press.code) {
                case 'KeyH':
                buttons.forEach(buttom => buttom.removeAttribute('style'));
                buttons.find(buttom => buttom.innerText === 'H').style.backgroundColor = '#9acd32';
                break;
                case 'KeyI':
                buttons.forEach(buttom => buttom.removeAttribute('style'));
                buttons.find(buttom => buttom.innerText === 'I').style.backgroundColor = 'red';
                break;
                case 'KeyS':
                buttons.forEach(buttom => buttom.removeAttribute('style'));
                buttons.find(buttom => buttom.innerText === 'S').style.backgroundColor = 'blue';
                break;
                case 'KeyU':
                buttons.forEach(buttom => buttom.removeAttribute('style'));
                buttons.find(buttom => buttom.innerText === 'U').style.backgroundColor = 'pink';
                break;
                case 'KeyN':
                buttons.forEach(buttom => buttom.removeAttribute('style'));
                buttons.find(buttom => buttom.innerText === 'N').style.backgroundColor = 'brown';
                break;
                case 'KeyD':
                buttons.forEach(buttom => buttom.removeAttribute('style'));
                buttons.find(buttom => buttom.innerText === 'D').style.backgroundColor = 'red';
                break;
                case 'KeyA':
                buttons.forEach(buttom => buttom.removeAttribute('style'));
                buttons.find(buttom => buttom.innerText === 'A').style.backgroundColor = 'blue';
                break;
                case 'KeyY':
                buttons.forEach(buttom => buttom.removeAttribute('style'));
                buttons.find(buttom => buttom.innerText === 'Y').style.backgroundColor = 'yellow';
                break;
                case 'Enter':
                buttons.forEach(btn => btn.removeAttribute('style'));
                buttons.find(btn => btn.innerText === 'Enter').style.backgroundColor = 'blue';
                break;
        }
    });
}

highligt();